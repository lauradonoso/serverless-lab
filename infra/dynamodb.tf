resource "aws_dynamodb_table" "registro_visitas" {
  name         = "registro-visitas"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "Fecha"
  range_key    = "Lugar"

  attribute {
    name = "Fecha"
    type = "S"
  }

  attribute {
    name = "Lugar"
    type = "S"
  }
}

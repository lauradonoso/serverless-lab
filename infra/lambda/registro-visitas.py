import boto3
import json

tableName = "registro-visitas"
dynamo = boto3.resource('dynamodb').Table(tableName)

print('Registrando visita...')

def handler(event, context):
    try:
        operation = event['operation']
        operations = {
            'create': ddb_create
        }

        if operation in operations:
            operations[operation](event.get('payload'))
            response = {
                "statusCode": 200,
                "body": json.dumps({
                    "message": "Operacion '{}' completada con exito.".format(operation),
                })
            }
        else:
            raise ValueError('Unrecognized operation "{}"'.format(operation))

    except Exception as e:
        response = {
            "statusCode": 500,
            "body": json.dumps({
                "message": str(e),
            })
        }

    return response

def ddb_create(x):
    dynamo.put_item(**x)

terraform {
  backend "s3" {
    bucket         = "state-375633710205"
    key            = "state/terraform.tfstate"
    region         = "us-east-2"
    dynamodb_table = "terraform-state"
  }
}


terraform {
 required_providers {
   aws = {
     source  = "hashicorp/aws"
     version = "~> 3.0"
   }
 }
}

provider "aws" {
  profile = "personal"
  region = var.region 
}

resource "aws_s3_bucket" "state-bucket" {
  bucket = "state-375633710205" 
  acl    = "private"

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.state-bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_dynamodb_table" "terraform-state" {
 name           = "terraform-state"
 read_capacity  = 20
 write_capacity = 20
 hash_key       = "LockID"

 attribute {
   name = "LockID"
   type = "S"
 }
}

resource "aws_s3_bucket" "lambda-bucket" {
  bucket = "lambda-bucket-375633710205"
  acl    = "private"
}


data "archive_file" "lambda_registros_visitas" {
  type = "zip"

  source_dir  = "${path.module}/lambda"
  output_path = "${path.module}/lambda.zip"
}

resource "aws_s3_bucket_object" "lambda_registros_visitas" {
  bucket = aws_s3_bucket.lambda-bucket.id

  key    = "lambda.zip"
  source = data.archive_file.lambda_registros_visitas.output_path

  etag = filemd5(data.archive_file.lambda_registros_visitas.output_path)
}

#CREACION DE LA FUNCION LAMBDA
resource "aws_lambda_function" "registros_visitas" {
  function_name = "registros-visitas"

  s3_bucket = aws_s3_bucket.lambda-bucket.id
  s3_key    = aws_s3_bucket_object.lambda_registros_visitas.key

  handler       = "registro-visitas.handler"
  runtime       = "python3.8"

  source_code_hash = data.archive_file.lambda_registros_visitas.output_base64sha256

  role = aws_iam_role.lambda_role.arn
}

resource "aws_cloudwatch_log_group" "registros_visitas" {
  name = "/aws/lambda/${aws_lambda_function.registros_visitas.function_name}"

  retention_in_days = 30
}


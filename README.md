# Prueba técnica DevOps

## Desarrollo del problema

Este proyecto muestra cómo se puede crear una función AWS Lambda utilizando Terraform. En este escenario, la función Lambda está diseñada para interactuar con una tabla DynamoDB y tener la capacidad de insertar registros en ella.

## Recursos utilizados
Los siguientes recursos de AWS se utilizan en este proyecto:

AWS Lambda Function: Es el servicio que ejecuta su código en respuesta a eventos, como cambios en los datos en un depósito de Amazon S3 o en una tabla de Amazon DynamoDB.

AWS DynamoDB Table: Se utilizó una tabla DynamoDB para almacenar los datos. La función Lambda tiene permisos para insertar registros en esta tabla.

AWS IAM Role and Policy: Para otorgar a la función Lambda los permisos necesarios para interactuar con la tabla DynamoDB, se creó un rol de IAM. Este rol tiene una política adjunta que permite la acción dynamodb:PutItem en el recurso DynamoDB.

También se crearon un usuario con permisos suficientes para ejecutar el pipeline.

AWS S3 Bucket: Se utilizó un bucket S3 para almacenar el archivo zip que contiene el código de la función Lambda. También se creó un bucket destinado a almacenar el state file del proyecto en Terraform.

AWS API Gateway: Se creó un API Gateway para exponer la función Lambda a través de una API HTTP. El API Gateway está configurado para invocar la función Lambda en respuesta a solicitudes HTTP. Contiene un método POST.

## Creación de la función Lambda
Para crear la función Lambda, se utilizó Terraform. La infraestructura como código (IaC) nos permite definir y proporcionar infraestructura de datos a través de archivos de configuración definidos en lenguaje declarativo. Se utilizó python 3.8 como runtime.

Para implementar estos recursos, simplemente ejecutamos los siguientes comandos en la terminal:

terraform init para inicializar nuestro entorno Terraform.
terraform validate para asegurar que nuestros archivos de configuración son válidos.
terraform plan para ver qué cambios se harán.
terraform apply para aplicar los cambios y crear los recursos.


### NOTA: el API gateway fue creado a traves de la consola de AWS, pero fue importado en el state file del proyecto utilizando la funcion terraform import.

## Despliegue de la aplicación
Para realizar el despliegue de la aplicacion, en principio se tiene propuesto realizarlo a traves del flujo que sigue Terraform:
- terraform init
- terrform validate
- terraform plan
- terraform apply

Una vez se ejecuta terraform apply, a través de data, un tipo de recurso que permite usar datos de una fuente externa dentro de la configuración de Terraform, se está creando un archivo zip a partir de la carpeta lambda que contiene el archivo .py. Este proceso realiza la misma función necesaria para desplegar la función lambda a AWS.

Para invocar la función lambda a través de HTTPS requests.
EJEMPLO:
curl https://4xclxetn25.execute-api.us-east-2.amazonaws.com/test/dynamodbinsert \
-d '{"operation": "create", "payload": {"Item": {"Fecha": "20-06-2023", "Lugar": "Calle 127 # 55-30"}}}'